/**
Copywrite 2016
Aren Moynihan

**/

// Define valuables that will be used in the game
var player;
var peices = [];
var score;
var curent_score = 0;
var life;
var lifeleft = 3;
var highscore = 0;
var playing=false;
var insturections;
var killer;

var canvassize = {
	canvas_height : window.innerHeight - 65,
	canvas_width : window.innerWidth - 22,
}


function startgame() {
	// Startup, creating the elements that will be present in the game
	// such as a player, a block to collct, and the text elements
	player = new component(20, 20, "green", Math.round((canvassize.canvas_width / 2) / 20) * 20, (Math.round((canvassize.canvas_height / 2) / 20) * 20));
	testpeice = new component(20, 20, "blue", Math.round((Math.random() * (canvassize.canvas_width - 40)) / 20) * 20, Math.round((Math.random() * (canvassize.canvas_height - 40)) / 20) * 20);
	killer = new component(20, 20, "red", 0, 0)
	score = new component("30px", "Consolas", "black", canvassize.canvas_width - 200, 40, "text");
	life = new component("30px", "Consolas", "black", 20, 40, "text");
	killer.takinglife = false;
	killer.speed = 5;
	gamearea.start();
	gamearea.canvas.focus();
	update();
	// switch playing between true and false when the mouse is clicked using jquery
	$(document).click(function (e) {
		if (playing == false){
			playing=true;
		}
		else {
			playing=false;
		}
	})
	// add all key pad functionality
	$(document).keydown(function (e) {
		switch(e.which) {
			case 37:
			moveleft();
			break;
			
			case 38:
			moveup();
			break;
			
			case 39:
			moveright();
			break;
			
			case 40:
			movedown();
			break;
			
			case 32:
			if (playing == false){
			 playing=true;
			}
			else {
				playing=false;
			}
			break;
			
			default: return;
		}
	e.preventDefault();
	});
}


var gamearea = {
	// creat a canvas
	canvas : document.createElement("canvas"),
	start : function() {
		// Set canvas properties
		this.canvas.width = canvassize.canvas_width;
		this.canvas.height = canvassize.canvas_height;
		this.context = this.canvas.getContext("2d");
		document.body.insertBefore(this.canvas, document.body.childNodes[0]);
	},
	clear : function() {
		// clear the game area
		this.context.clearRect(0,0,this.canvas.width, this.canvas.height);
	}
}

// Crate all game pieces
function component(width, height, color, x, y, type) {
	// Define most of the valuables that will be used for the piece and set some parameters for the piece
	this.width = width;
	this.height = height;
	this.x = x;
	this.y = y;
	this.type = type;
	this.speedx = 0;
	this.speedy = 0;
	this.blank = false;
	this.update = function () {
		//Render the pies
		ctx = gamearea.context;
		if (this.type == "text") {
			ctx.font = this.width + " " + this.height;
			ctx.fillStyle = color;
			ctx.fillText(this.text, this.x, this.y);
		}
		else {
			ctx.fillStyle = color;
			// Only Render a new frame if this.blank isn't true
			if (this.blank == false) {
   			ctx.fillRect(this.x, this.y, this.width, this.height);
   	}
   	}
   }
   this.newpos = function () {
   	//Move the pies, only used for the player
  		this.x += this.speedx;
  		this.y += this.speedy;
   	this.speedx = 0;
   	this.speedy = 0;
   }
   this.automove = function (towdard) {
   	if ((towdard.x - this.x > 0) && (towdard.x - this.x > towdard.y - this.y)){
   		this.speedx = this.speed;
   	}
   	
   	if ((towdard.x - this.x < 0) && (towdard.x - this.x < towdard.y - this.y)){
   		this.speedx = this.speed * -1;
   	}
   	
   	if ((towdard.y - this.y < 0) && (towdard.y - this.y < towdard.x - this.x)){
   		this.speedy = this.speed * -1;
   	}
   	
   	if ((towdard.y - this.y > 0) && (towdard.y - this.y > towdard.x - this.x)){
   		this.speedy = this.speed;
   	}
   }
	this.crashwith = function(otherobj) {
		// Test to see if two pieces are touching
		if (this.blank == false) {
			var myleft = this.x;
			var myright = this.x + (this.width);
			var mytop = this.y;
			var mybottom = this.y + (this.height);
			var otherleft = otherobj.x;
			var otherright = otherobj.x + (otherobj.width);
			var othertop = otherobj.y;
			var otherbottom = otherobj.y + (otherobj.height);
			var crash = true;
			if ((mybottom < othertop) ||
				(mytop > otherbottom) ||
				(myright < otherleft) ||
				(myleft > otherright)) {
				crash = false;
			}
			return crash;
		}
	}
	this.remove = function () {
		// Set this.blank to true so that the script knows not too render the object
		this.blank = true;
	}

}

function updatecl() {
	//Function to update the game area if the variable playing is true
	// called once every second
	if (playing == true){
		time -= 1;
		update();
	}
}

function update() {
	// Updates renders a new frame, and checks to see if the player got a point or if the time is up
	if (playing == true) {
		player.newpos();
		if (player.crashwith(killer)) {
			if (killer.blank == false) {
				if (killer.takinglife == false) {
					lifeleft -= 1;
					killer.takinglife = true;
				}
				else {
					killer.takinglife = false;
				}
			}
		}
	}
	if (lifeleft <= 0){
		if (curent_score > highscore) {
			alert("You died, congradulations you got a new high score of " + curent_score + "\n" + "your old high score was " + highscore +" press ok to play a new round ");
			console.log('1');
			newround();
		}
		else {
			alert("you died your score is " + curent_score + "\n" + "you're high score is " + highscore + " press ok to play a new round ");
			console.log('2')
			newround();
		}
	} 

	if (player.crashwith(testpeice)) {
		if (testpeice.blank == false) {
			testpeice.x = Math.round((Math.random() * (canvassize.canvas_width - 40)) / 20) * 20;
			testpeice.y = Math.round((Math.random() * (canvassize.canvas_height - 40)) / 20) * 20;
			testpeice.newpos();
			curent_score += 1;
			if (curent_score == 10 ){
				killer.speed += 5;
			}
			if (curent_score == 15) {
				killer.speed += 2;
			}
			if (curent_score == 20) {
				killer.speed += 2;
			}
			if ((curent_score > 24) && (curent_score < 51)) {
				killer.speed += 1;
				player.speed += .9;
			}
		}
	}
	gamearea.clear();
	testpeice.update();
	player.update();
	score.text="SCORE: " + curent_score;
	score.update();
	life.text="LIFE: " + lifeleft;
	life.update();
	if (playing == true){
		killer.automove(player);
		killer.newpos();
	}
	killer.update();
}

function newround() {
	// Clears all the valuables and updates the high score to reset the round  
	if (curent_score > highscore) {
		console.log('3');
		console.log("new high score!");
		highscore = curent_score;
	}
	console.log("new round");
	killer.x = 0;
	killer.y = 0;
	player.x =  Math.round((canvassize.canvas_width / 2) / 20) * 20;
	player.y =  Math.round((canvassize.canvas_height / 2) / 20) * 20;
	lifeleft = 3;
	curent_score = 0;
}

// Functions to move the player
function moveup() {
	player.speedy = -20;
	update();
}

function movedown() {
	player.speedy = 20;
	update();
}

function moveleft() {
	player.speedx = -20;
	update();
}
function moveright() {
	player.speedx = 20;
	update();
}

startgame();