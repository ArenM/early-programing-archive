#! /usr/lib/python3.4

from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator

ID = 'test'

def main():
    ind = appindicator.Indicator.new(ID, 'test', appindicator.IndicatorCategory.SYSTEM_SERVICES)
    ind.set_status(appindicator.IndicatorStatus.ACTIVE)
    ind.set_menu(main_menu())
    gtk.main()
def main_menu():
    menu = gtk.Menu()
    item_quit = gtk.MenuItem('quit')
    item_quit.connect('activate', lambda exit())
    menu.append(item_quit)
    menu.show_all()
    return menu

if __name__ == '__main__':
    main()
