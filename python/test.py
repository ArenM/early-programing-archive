#! /usr/bin/python3

import tkinter as tk
    
hedder = ('ariel', 12)
class test(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)
        container = tk.Frame(self)
        container.pack(side='top', fill="both", expand = True)
        self.title("hooray")
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.frames = {}
        for F in (StartPage, PageOne, PageTwo):
            frame = F(self, container)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
#        StartPage.tkraise(container)
        self.show_frame(StartPage)
    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class StartPage(tk.Frame):
    def __init__(self, parent, container):
        tk.Frame.__init__(self, container)
        lable = tk.Label(self, text="start page", font=hedder)
        lable.pack(pady=2, padx=2)
        button = tk.Button(self, text="page two", command=lambda: parent.show_frame(PageTwo))
        button1 = tk.Button(self, text="page one", command=lambda: parent.show_frame(PageOne))
        button1.pack()
        button.pack(pady=2, padx=2)

class PageOne (tk.Frame):
    def __init__(self, parent, container):
        tk.Frame.__init__(self, container)
        label = tk.Label(self, text="page one", font=hedder)
        button = tk.Button(self, text="StartPage", command=lambda: parent.show_frame(StartPage))
        button1 = tk.Button(self, text="page two", command=lambda: parent.show_frame(PageTwo))
        label.pack(pady=2, padx=2)
        button.pack()
        button1.pack()

class PageTwo(tk.Frame):
    def __init__(self, parent, container):
        tk.Frame.__init__(self, container)
        lable = tk.Label(self, text="page two", font=hedder)
        lable.pack(pady=2, padx=2)
        button = tk.Button(self, text="StartPage", command=lambda: parent.show_frame(StartPage))
        button1 = tk.Button(self, text="page one", command=lambda: parent.show_frame(PageOne))
        button1.pack()
        button.pack()

test().mainloop()
