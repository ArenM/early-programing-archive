#! /usr/bin/python3

## 
## --- Initialization ---
##

#imports
from tkinter import *
import traceback
#import battery-life-calculator-GUI_def-simple

#open a GUI window
mainwindow = Tk()
window = Frame(mainwindow)
window.grid(row=0,column=0,sticky='nw')

## define functions
def clear():
    try:
        err.grid_forget()
    except:
        pass
    try:
        result.grid_forget()
    except:
        pass



def caluate(typ):
    try:
        while True:
            global err
            global numb1
            global numb2
            global numb3
            global result
            clear()
            
            try:
                numb1 = int(num1.get())
                if numb1 <= 0:
                    clear()
                    err = Label(window, text="the copasicity must be grater than 0",fg="red")
                    err.grid(row=4,column=0,sticky='nw')
                    break
            except:
                clear()
                err = Label(window, text="the capicity you enterd isn't a number try again",fg="red")
                err.grid(row=4,column=0,sticky='nw')
                break
            
            try:
                numb2 = int(num2.get())
                if numb2 <= 0:
                    clear()
                    err = Label(window, text="the draw must be grater than 0",fg="red")
                    err.grid(row=4,column=0,sticky='nw')
                    break
            except:
                err = Label(window, text="the draw you enterd isn't a number try again",fg="red")
                err.grid(row=4,column=0,sticky='nw')
                break
            
            try:
                numb3 = int(num3.get())
                if numb3 < 0:
                    clear()
                    err = Label(window, text="the effincyloss must be grater than or equal to 0",fg="red")
                    err.grid(row=4,column=0,sticky='nw')
                    break
                if numb3 >= 100:
                    clear()
                    err = Label(window, text="the effincyloss must be lessthan 100",fg="red")
                    err.grid(row=4,column=0,sticky='nw')
                    break
                if numb3 != 0:
                    numb3 = numb3/10
                numb3 = numb3+10
            except:
                clear()
                err = Label(window, text="the effincyloss must be grater than or equal to 0",fg="red")
                err.grid(row=4,column=0,sticky='nw')
                break

            show = numb1/numb2*numb3/10
            minutes = show*60
            hour = minutes
            if show > 1:
                while True:
                    if minutes >= 60:
                        minutes = minutes - 60
                    else:
                        break
            hour = hour-minutes
            hour = hour/60
            if hour == 1:
                hours = " hour "
            else:
                hours = " hours "
            
            if minutes == 1:
                minute = " minute "
            else:
                minute = " minutes "

            if typ == 'run':
                if hour == 0:
                    result = Label(window, text="it will last for " + str(minutes) + minute)
                elif hour != 0 and minutes == 0:
                    result = Label(window, text="it will last for " + str(hour) + hours)
                else:
                    result = Label(window, text="it will last for " + str(hour) + hours + str(minutes) + minute)
            if typ == 'charge':
                if hour == 0:
                    result = Label(window, text="it will take " + str(minutes) + minute + "to charge")
                elif hour != 0 and minutes == 0:
                    result = Label(window, text="it will take " + str(hour) + hours + "to charge")
                else:
                    result = Label(window, text="it will take " + str(hour) + hours + str(minutes) + minute + "to charge")
            result.grid(row=3,padx=60,sticky='nw')
            break
    except:
        width = mainwindow.winfo_width()
        clear()
        err = Label(mainwindow, text='the program has expirences this error:\n\n' + str(traceback.format_exc()) + '\nyou might be able to fix the problem by changing the data fields',fg="red", wraplength=width)
        err.grid(row=1,column=0,sticky='nw')




## 
## --- Main block to open windows and get input ---
##

num1 = IntVar()
num2 = IntVar()
num3 = IntVar()

#add labels in the window
numbe1 = Label(window, text='capacity in mAh').grid(row=0,column=0,sticky="nw")
numbe2 = Label(window, text='draw or charage rate').grid(row=1,column=0,sticky="nw")
numbe3 = Label(window, text='effincy loss').grid(row=2,column=0,sticky='nw')

#add textboxes to receive user input
numbr1 = Entry(window, textvariable=num1).grid(row=0,column=1,sticky="nw")
numbr2 = Entry(window, textvariable=num2).grid(row=1,column=1,sticky="nw")
numbr3 = Entry(window, textvariable=num3).grid(row=2,column=1,sticky='nw')

#add buttons to receive user commands
calc_run = Button(window, text="caluate runtime", command=lambda: caluate('run')).grid(row=3,column=1)
calc_charg = Button(window, text="caluate chargetime", command=lambda: caluate('charge')).grid(row=4,column=1)
cle = Button(window, text="clear", command=clear).grid(row=5,column=1)

#clean up the window size and add titles
mainwindow.geometry("500x200")
mainwindow.title("battery life calculator")
#mainwindow.icon
mainwindow.mainloop()
